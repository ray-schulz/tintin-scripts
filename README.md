tintin-scripts
==============

This is my collection of tintin++ scripts for WoTMUD.

I have changed the format of my scripts, now you can copy/paste directly into tintin++.

If you're having issues with colors or `#highlight` then set packet patch to something: 
`#config {packet patch} {.17}` Mess around with it until you find the number you like.

`#line` has some issues with tintin-2.00.9, either use 2.00.8 or 2.01.0 beta 
for now if you need `#line` to function properly.
